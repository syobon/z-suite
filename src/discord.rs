use std::{cmp::Ordering, error::Error, sync::OnceLock};

use ureq::{json, request, Request};

use crate::participants::get_id;

#[derive(Debug)]
pub struct Config {
    pub url: String,
    pub thread_id: Option<u64>,
}

enum Color {
    Black = 2_303_786,
    White = 16_777_215,
    Red = 16_711_680,
    Blue = 255,
}

pub static CONFIG: OnceLock<Config> = OnceLock::new();

pub fn post_metadata(count: usize, author: &str) -> Result<(), Box<dyn Error>> {
    let json = build_embed(
        Color::Black,
        &format!("**第{count}問**"),
        &format!("**作問者**  {author}"),
    );
    let request = build_request("POST", None, false);

    request.send_json(json)?;

    Ok(())
}

pub fn send_question(message_id: Option<u64>, text: &str) -> Result<Option<u64>, Box<dyn Error>> {
    let json = build_embed(Color::Black, "問題", text);
    let request = if message_id.is_some() {
        build_request("PATCH", message_id, false)
    } else {
        build_request("POST", None, true)
    };

    let response = request.send_json(json)?;
    if message_id.is_none() {
        let response_json: serde_json::Value = response.into_json()?;
        let id = response_json["id"]
            .as_str()
            .ok_or("Failed to parse response")?
            .parse()?;
        Ok(Some(id))
    } else {
        Ok(None)
    }
}

pub fn post_ready(name: &str) -> Result<(), Box<dyn Error>> {
    let id = get_id(name)?;
    let json = build_embed(Color::White, "解答", &format!("<@{id}> 解答をどうぞ"));
    let request = build_request("POST", None, false);

    request.send_json(json)?;

    Ok(())
}

pub fn post_score_change(name: &str, score: i32, is_answer: bool) -> Result<(), Box<dyn Error>> {
    let id = get_id(name)?;
    let json = match score.cmp(&0) {
        Ordering::Greater => build_embed(
            Color::Red,
            if is_answer { "正解" } else { "得点調整" },
            &format!("<@{id}> {score}点獲得"),
        ),
        Ordering::Equal => build_embed(
            Color::Blue,
            if is_answer {
                "不正解"
            } else {
                "得点調整"
            },
            "減点なし",
        ),
        Ordering::Less => build_embed(
            Color::Blue,
            if is_answer {
                "不正解"
            } else {
                "得点調整"
            },
            &format!("<@{id}> {}点減点", -score),
        ),
    };
    let request = build_request("POST", None, false);

    request.send_json(json)?;

    Ok(())
}

pub fn post_message(mes_type: &str, answer: &str) -> Result<(), Box<dyn Error>> {
    let json = build_embed(Color::White, mes_type, answer);
    let request = build_request("POST", None, false);

    request.send_json(json)?;

    Ok(())
}

pub fn post_result(result: Vec<(u64, i64)>) -> Result<(), Box<dyn Error>> {
    let fields: Vec<serde_json::Value> = result
        .into_iter()
        .enumerate()
        .map(|res| {
            json!({
                "name": format!("第{}位", res.0 + 1),
                "value": format!("{}点　　　<@{}>", res.1.1, res.1.0)
            })
        })
        .collect();
    let json = json!({
        "embeds" : [{
            "color": Color::White as u32,
            "title": "結果発表",
            "fields": fields
        }]
    });

    let request = build_request("POST", None, false);

    request.send_json(json)?;

    Ok(())
}

fn build_request(method: &str, message_id: Option<u64>, wait: bool) -> Request {
    let config = CONFIG.get().unwrap();
    let url = message_id.map_or_else(
        || config.url.clone(),
        |message_id| format!("{}/messages/{message_id}", config.url),
    );

    let mut request = request(method, &url);
    if let Some(thread_id) = config.thread_id {
        request = request.query("thread_id", &thread_id.to_string());
    }
    if wait {
        request = request.query("wait", "true");
    }
    request
}

fn build_embed(color: Color, title: &str, desc: &str) -> ureq::serde_json::Value {
    json!({
        "embeds" : [{
            "color": color as u32,
            "title": title,
            "description": desc,
        }]
    })
}
