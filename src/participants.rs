use std::error::Error;

use rusqlite::Connection;

pub fn add(name: String, id: String) -> Result<(), Box<dyn Error>> {
    let db = crate::DB.get().unwrap();
    let conn = Connection::open(db)?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS participants (
            name TEXT PRIMARY KEY,
            id INTEGER UNIQUE
        )",
        (),
    )?;
    conn.execute(
        "INSERT INTO participants (name, id) VALUES (?1, ?2)",
        (name, id),
    )?;
    Ok(())
}

pub fn remove(name: String) -> Result<(), Box<dyn Error>> {
    let db = crate::DB.get().unwrap();
    let conn = Connection::open(db)?;
    conn.execute("DELETE FROM participants WHERE name = ?1", [name])?;
    Ok(())
}

pub fn list() -> Result<Vec<String>, Box<dyn Error>> {
    let db = crate::DB.get().unwrap();
    let conn = Connection::open(db)?;
    let mut stmt = conn.prepare("SELECT name FROM participants")?;
    let participants: Vec<String> = stmt
        .query_map([], |row| row.get(0))?
        .filter_map(std::result::Result::ok)
        .collect();
    Ok(participants)
}

pub fn get_id(name: &str) -> Result<String, Box<dyn Error>> {
    let db = crate::DB.get().unwrap();
    let conn = Connection::open(db)?;
    let id: u64 = conn.query_row(
        "SELECT id FROM participants WHERE name = ?1",
        [name],
        |row| row.get(0),
    )?;
    Ok(id.to_string())
}

pub fn score(name: &str, score: i32) -> Result<(), Box<dyn Error>> {
    let db = crate::DB.get().unwrap();
    let conn = Connection::open(db)?;
    conn.execute(
        "INSERT INTO scoreboard (name, score) VALUES (?1, ?2)",
        (name, score),
    )?;
    Ok(())
}

pub fn get_result() -> Result<Vec<(u64, i64)>, Box<dyn Error>> {
    let db = crate::DB.get().unwrap();
    let conn = Connection::open(db)?;
    let mut stmt = conn.prepare("SELECT id, \"sum(score)\" FROM result")?;
    let result = stmt
        .query_map([], |row| Ok((row.get(0)?, row.get(1)?)))?
        .map(|row| row.unwrap())
        .collect();

    Ok(result)
}
