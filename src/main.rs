use std::{path::PathBuf, sync::OnceLock};

use clap::{Parser, Subcommand};
use utils::readline;

mod converter;
mod discord;
mod participants;
mod run;
mod utils;

pub static DB: OnceLock<PathBuf> = OnceLock::new();

#[derive(Parser)]
#[clap(
    version = env!("CARGO_PKG_VERSION"),
    author = env!("CARGO_PKG_AUTHORS"),
    about = env!("CARGO_PKG_DESCRIPTION"),
)]
struct Arg {
    #[clap(short, value_name = "SQLITE_FILE", default_value = "./database.db")]
    db: PathBuf,
    #[clap(subcommand)]
    subcommand: SubCommand,
}

#[derive(Subcommand)]
enum SubCommand {
    Import {
        #[clap(value_name = "CSV_FILE")]
        path: PathBuf,
        #[clap(long = "question", value_name = "HEADER_QUESTION")]
        header_question: Option<String>,
        #[clap(long = "author", value_name = "HEADER_AUTHOR")]
        header_author: Option<String>,
        #[clap(long = "answer", value_name = "HEADER_ANSWER")]
        header_answer: Option<String>,
        #[clap(long = "tips", value_name = "HEADER_TIPS")]
        header_tips: Option<String>,
    },
    Participants {
        #[clap(subcommand)]
        command: ParticipantsCommand,
    },
    Run {
        #[clap(short, long = "seed", value_name = "SEED")]
        seed: Option<u64>,
        #[clap(long = "resume", value_name = "QUESTION_NUMBER")]
        resume: Option<usize>,
    },
}

#[derive(Subcommand)]
enum ParticipantsCommand {
    Add {
        #[clap(value_name = "NAME")]
        name: String,
        #[clap(value_name = "ID")]
        id: String,
    },
    Remove {
        #[clap(value_name = "NAME")]
        name: String,
    },
}

fn main() {
    let args = Arg::parse();
    DB.set(args.db).unwrap();
    let (action, result) = match args.subcommand {
        SubCommand::Import {
            path,
            header_question,
            header_author,
            header_answer,
            header_tips,
        } => {
            let header_question =
                header_question.unwrap_or_else(|| readline("Header name of question field"));
            let header_author =
                header_author.unwrap_or_else(|| readline("Header name of author field"));
            let header_answer =
                header_answer.unwrap_or_else(|| readline("Header name of answer field"));
            let header_tips = header_tips.unwrap_or_else(|| readline("Header name of tips field"));
            (
                "imported questions",
                converter::convert(
                    path,
                    &header_question,
                    &header_author,
                    &header_answer,
                    &header_tips,
                ),
            )
        }
        SubCommand::Participants { command } => match command {
            ParticipantsCommand::Add { name, id } => {
                ("added a participant", participants::add(name, id))
            }
            ParticipantsCommand::Remove { name } => {
                ("removed a participant", participants::remove(name))
            }
        },
        SubCommand::Run { seed, resume } => (
            "finished the operation",
            run::start(seed, resume.unwrap_or(1) - 1),
        ),
    };
    if let Err(e) = result {
        eprintln!("* Error occured: {e:?}");
    } else {
        println!("* Successfully {action}.");
    }
}
