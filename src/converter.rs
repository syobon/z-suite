use std::{error::Error, fs::File, path::PathBuf};

use rusqlite::Connection;

pub fn convert(
    csv: PathBuf,
    header_question: &str,
    header_author: &str,
    header_answer: &str,
    header_tips: &str,
) -> Result<(), Box<dyn Error>> {
    let db = crate::DB.get().unwrap();

    let mut index_question = None;
    let mut index_author = None;
    let mut index_answer = None;
    let mut index_tips = None;

    let f = File::open(csv)?;
    let mut rdr = csv::Reader::from_reader(f);
    for (i, header) in rdr.headers()?.iter().enumerate() {
        match header {
            h if h == header_question => {
                index_question = Some(i);
            }
            h if h == header_author => {
                index_author = Some(i);
            }
            h if h == header_answer => {
                index_answer = Some(i);
            }
            h if h == header_tips => {
                index_tips = Some(i);
            }
            _ => {}
        }
    }

    let index_question = index_question.ok_or("Question header not found")?;
    let index_author = index_author.ok_or("Author header not found")?;
    let index_answer = index_answer.ok_or("Answer header not found")?;
    let index_tips = index_tips.ok_or("Tips header not found")?;

    let conn = Connection::open(db)?;
    conn.execute("DROP TABLE IF EXISTS questions", ())?;
    conn.execute(
        "CREATE TABLE questions (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            author TEXT,
            text TEXT,
            answer TEXT,
            tips TEXT
        )",
        (),
    )?;

    for result in rdr.records() {
        let record = result?;
        conn.execute(
            "INSERT INTO questions (author, text, answer, tips) VALUES (?1, ?2, ?3, ?4)",
            (
                record.get(index_author),
                record
                    .get(index_question)
                    .map(|text| text.replace(r"\n", "\n")),
                record.get(index_answer),
                record.get(index_tips).map(|text| text.replace(r"\n", "\n")),
            ),
        )?;
    }

    Ok(())
}
