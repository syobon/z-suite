use std::{
    error::Error,
    fmt::Display,
    io::{stdin, stdout, Write},
};

use crate::discord::{post_ready, post_score_change};

pub enum Message {
    Answered,
    Refresh,
    Exit,
}

pub fn readline(prompt: impl Display) -> String {
    print!("{prompt}: ");
    stdout().flush().expect("IO Error.");
    let mut buffer = String::new();
    stdin().read_line(&mut buffer).expect("IO Error.");
    buffer.trim().to_owned()
}

pub fn prompt() -> Result<Message, Box<dyn Error>> {
    loop {
        let input = readline("");
        match input.as_str() {
            "ans" | "answer" => {
                let participants = crate::participants::list()?.join(", ");
                println!("{participants}");
                let name = readline("Name");
                post_ready(&name)?;
                let Ok(score) = readline("Score").parse::<i32>() else {
                    continue;
                };
                crate::participants::score(&name, score)?;
                post_score_change(&name, score, true)?;
            }
            "score" => {
                let participants = crate::participants::list()?.join(", ");
                println!("{participants}");
                let name = readline("Name");
                let Ok(score) = readline("Score").parse::<i32>() else {
                    continue;
                };
                crate::participants::score(&name, score)?;
                post_score_change(&name, score, false)?;
            }
            "re" | "refresh" => {
                return Ok(Message::Refresh);
            }
            "skip" => {
                return Ok(Message::Answered);
            }
            "" => break,
            _ => continue,
        }
    }
    Ok(Message::Exit)
}
