use rand::prelude::*;
use rand_chacha::ChaCha8Rng;
use rusqlite::Connection;
use std::error::Error;

use crate::{
    discord::{post_message, post_metadata, post_result, send_question},
    participants::get_result,
    utils::{prompt, readline, Message},
};

struct Question {
    author: String,
    text: String,
    answer: String,
    tips: String,
}

pub fn start(seed: Option<u64>, start_from: usize) -> Result<(), Box<dyn Error>> {
    let db = crate::DB.get().unwrap();
    let seed = seed.unwrap_or_else(|| {
        let mut rng = thread_rng();
        rng.gen()
    });
    println!("Using seed: {seed}");
    let mut rng = ChaCha8Rng::seed_from_u64(seed);

    let conn = Connection::open(db)?;

    conn.execute(
        "CREATE TABLE IF NOT EXISTS scoreboard (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            score INTEGER
        )",
        (),
    )?;

    conn.execute(
        "CREATE VIEW IF NOT EXISTS result AS
        SELECT participants.id, sum(score)
        FROM scoreboard
        INNER JOIN participants ON scoreboard.name = participants.name
        GROUP BY scoreboard.name
        ORDER BY sum(score) DESC",
        (),
    )?;

    let mut stmt = conn.prepare("SELECT author, text, answer, tips FROM questions")?;
    let mut questions: Vec<Question> = stmt
        .query_map([], |row| {
            Ok(Question {
                author: row.get(0)?,
                text: row.get(1)?,
                answer: row.get(2)?,
                tips: row.get(3)?,
            })
        })?
        .filter_map(std::result::Result::ok)
        .collect();

    questions.shuffle(&mut rng);

    let url = std::env::var("Z_WEBHOOK").unwrap();
    let thread_id = std::env::var("Z_THREAD").map(|id| id.parse().ok()).unwrap();
    let config = crate::discord::Config { url, thread_id };
    crate::discord::CONFIG.set(config).unwrap();

    for (i, question) in questions.into_iter().skip(start_from).enumerate() {
        let q_counter = i + start_from + 1;

        println!("--- BODY ---");
        println!("{}", question.text);
        println!("------------");
        println!("-- ANSWER --");
        println!("{}", question.answer);
        println!("------------");
        println!("--- TIPS ---");
        println!("{}", question.tips);
        println!("------------");

        post_metadata(q_counter, &question.author)?;

        readline("Hit Enter key to continue");

        let mut sentences = question.text.split('/');
        let mut text = String::from(sentences.next().unwrap());
        let mut message_id = None;
        let mut answered = false;
        if let Some(last) = sentences.next_back() {
            message_id = send_question(None, &format!("{text}/"))?;
            for sentence in sentences {
                while !answered {
                    match prompt()? {
                        Message::Refresh => message_id = send_question(None, &format!("{text}/"))?,
                        Message::Exit => break,
                        Message::Answered => {
                            answered = true;
                            break;
                        }
                    }
                }
                text += sentence;
                if !answered {
                    send_question(message_id, &format!("{text}/"))?;
                }
            }
            text += last;
            while !answered {
                match prompt()? {
                    Message::Refresh => message_id = send_question(None, &format!("{text}"))?,
                    Message::Exit => break,
                    Message::Answered => {
                        answered = true;
                    }
                }
            }
        }
        if answered {
            post_message("問題", &text)?;
        } else {
            send_question(message_id, &text)?;
            while matches!(prompt()?, Message::Refresh) {
                send_question(None, &format!("{text}"))?;
            }
        }
        post_message("模範解答", &question.answer)?;
        if !question.tips.is_empty() {
            post_message("備考", &question.tips)?;
        }
        readline("Hit Enter key to continue");
    }

    let result = get_result()?;
    post_result(result)?;

    Ok(())
}
